import os

from oslo_config import cfg

from beedb_client.common import cli_application
from beedb_client.common import print_utils
from beedb_client.v1.vcloudair import client


class Databases(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--description', dest='description',
                          help="Database description.")
    @cli_application.args('--role-name', dest='database role')
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def create(self, beedb_endpoint=None, database=None,
               role=None, insecure=None, description=None, username=None,
               password=None, service_type=None, vcloud_host=None,
               service_version=None, vcloud_instance=None,
               vcloud_org_name=None, vcloud_org_service=None,
               ):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_dict(bee.databases.create(
            database,
            role,
            description=description).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def list(self, beedb_endpoint=None,
             insecure=None, username=None,
             password=None, service_type=None,
             vcloud_host=None, service_version=None,
             vcloud_instance=None, vcloud_org_name=None,
             vcloud_org_service=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_list(bee.databases.list(),
                               ['name', 'status',
                                'tenant', 'database_type',
                                'url', 'created_at',
                                'updated_at'])

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def get(self, beedb_endpoint=None,
            insecure=None, username=None,
            password=None, service_type=None,
            vcloud_host=None, service_version=None,
            vcloud_instance=None, vcloud_org_name=None,
            vcloud_org_service=None, database=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_dict(
            bee.databases.get(database).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--database', dest='database',
                          help='Database identifier')
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def delete(self, beedb_endpoint=None,
               insecure=None, username=None,
               password=None, service_type=None,
               vcloud_host=None, service_version=None,
               vcloud_instance=None, vcloud_org_name=None,
               vcloud_org_service=None, database=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        bee.databases.delete(database)
        print("OK. Done.")


class Roles(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def list(self, beedb_endpoint=None,
             insecure=None, username=None,
             password=None, service_type=None,
             vcloud_host=None, service_version=None,
             vcloud_instance=None, vcloud_org_name=None,
             vcloud_org_service=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_list(bee.roles.list(),
                               ['name', 'status',
                                'tenant', 'database_type',
                                'password', 'created_at',
                                'updated_at'])

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--role-name', dest='role',
                          help='Role to seek for')
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def get(self, beedb_endpoint=None,
            insecure=None, username=None,
            password=None, service_type=None,
            vcloud_host=None, service_version=None,
            vcloud_instance=None, vcloud_org_name=None,
            vcloud_org_service=None, role=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_dict(
            bee.roles.get(role).json_object)

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def delete(self, beedb_endpoint=None,
               insecure=None, username=None,
               password=None, service_type=None,
               vcloud_host=None, service_version=None,
               vcloud_instance=None, vcloud_org_name=None,
               vcloud_org_service=None, role=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        bee.roles.delete(role)
        print("OK. Done.")

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--role-name', dest='role',
                          help='Role name')
    @cli_application.args('--role-password', dest='role_password',
                          help='Role name password')
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--description', dest='description',
                          help="Database description.")
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    @cli_application.args('--database-type', dest='database_type',
                          help="Role for database assigning.")
    def create(self, beedb_endpoint=None, role_password=None,
               role=None, insecure=None, description=None, username=None,
               password=None, service_type=None, vcloud_host=None,
               service_version=None, vcloud_instance=None,
               vcloud_org_name=None, vcloud_org_service=None,
               database_type=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_dict(bee.roles.create(
            role,
            role_password,
            database_type,
            description=description).json_object)


class DatabaseTypes(object):

    @cli_application.args('--beedb-endpoint', dest="beedb_endpoint",
                          help='BeeDB API endpoint',
                          default=os.environ.get('BEEDB_ENDPOINT'))
    @cli_application.args('--insecure', dest='insecure',
                          help="Enable SSL verification.",
                          default=False)
    @cli_application.args('--username', dest='username',
                          help="vCloud username",
                          default=os.environ.get('VCLOUD_USERNAME'))
    @cli_application.args('--password', dest='password',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_PASSWORD'))
    @cli_application.args('--service-type', dest='service_type',
                          help="vCloud user password",
                          default=os.environ.get('VCLOUD_SERVICE_TYPE'))
    @cli_application.args('--vcloud-host', dest='vcloud_host',
                          default=os.environ.get('VCLOUD_HOST'),
                          help="vCloud host")
    @cli_application.args('--service-version', dest='service_version',
                          default=os.environ.get("VCLOUD_SERVICE_VERSION"),
                          help="vCloud service version")
    @cli_application.args('--vcloud-instance', dest='vcloud_instance',
                          default=os.environ.get('VCLOUD_INSTANCE'),
                          help="vCloud account instance")
    @cli_application.args('--vcloud-organization', dest='vcloud_org_name',
                          default=os.environ.get('VCLOUD_ORG_NAME'),
                          help='vCloud account organization')
    @cli_application.args('--vcloud-organization-service',
                          dest='vcloud_org_service',
                          default=os.environ.get('VCLOUD_ORG_SERVICE'),
                          help='vCloud organization service')
    def list(self, beedb_endpoint=None,
             insecure=None, username=None,
             password=None, service_type=None,
             vcloud_host=None, service_version=None,
             vcloud_instance=None, vcloud_org_name=None,
             vcloud_org_service=None):
        bee = client.vCloudAirBeeDBClient(
            beedb_endpoint,
            username,
            password,
            vcloud_host,
            service_type,
            service_version,
            vcloud_instance,
            vcloud_org_service,
            vcloud_org_name,
            insecure=insecure,
            enable_logging=False,
        )
        print_utils.print_list(bee.database_types.list(),
                               ['name', 'description',
                                'created_at', 'updated_at'])


CATS = {
    'databases': Databases,
    'roles': Roles,
}

category_opt = cfg.SubCommandOpt(
    'category',
    title='Command categories',
    help='Available categories',
    handler=cli_application.add_command_parsers(CATS)
)


def main():
    """Parse options and call the appropriate class/method."""
    cli_application.main(cfg.CONF, category_opt)

if __name__ == "__main__":
    main()
