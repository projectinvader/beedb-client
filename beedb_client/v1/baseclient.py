import requests

try:
    import simplejson as json
except ImportError:
    import json

from beedb_client.common import exceptions
from beedb_client.common import log as logging


class _HTTPClient(object):

    def _log_request(self, logger, data=None, headers=None):
        if logger is not None:
            if headers is not None:
                for header in headers:
                    logger.debug('request header: %s: %s',
                                 header, headers[header])
            if data is not None:
                logger.debug('request data:\n %s', data)

    def _log_response(self, logger, response):
        if logger is not None:
            logger.debug('[%d] %s', response.status_code, response.text)

    def _get(self, url, data=None, logger=None, **kwargs):
        if logger is not None:
            self._log_request(logger, data=data,
                              headers=kwargs.get('headers', None))
        response = requests.get(url, data=data, **kwargs)
        self._log_response(logger, response)
        return response

    def _post(self, url, data=None, json=None, logger=None, **kwargs):
        if logger is not None:
            self._log_request(logger, data=data,
                              headers=kwargs.get('headers', None))
        response = requests.post(url, data=data, json=json, **kwargs)
        self._log_response(logger, response)
        return response

    def _put(self, url, data=None, logger=None, **kwargs):
        if logger is not None:
            self._log_request(logger, data=data,
                              headers=kwargs.get('headers', None))
        response = requests.put(url, data=data, **kwargs)
        self._log_response(logger, response)
        return response

    def _delete(self, url, data=None, logger=None, **kwargs):
        if logger is not None:
            self._log_request(logger, data=data,
                              headers=kwargs.get('headers', None))
        response = requests.delete(url, data=data, **kwargs)
        self._log_response(logger, response)
        return response


class BeeDBClient(object):

    def __init__(self, beedb_endpoint,
                 auth_headers=None,
                 verify=True,
                 enable_log=False):
        self.beedb_endpoint = beedb_endpoint
        self._headers = auth_headers
        self.logger = logging.get_logger() if enable_log else None
        self.verify = verify
        self.recent_response = None

        self.databases = Databases(self, enable_logging=enable_log)
        self.roles = Roles(self, enable_logging=enable_log)
        self.database_types = DatabaseTypes(self, enable_logging=enable_log)

    @property
    def tenant(self):
        """Unified wrapper for cloud user identifier.
           Example:
               OpenStack clouds: Tenant ID (in future: Project ID)
               vCloud Air: Org-ID
        :return:
        """
        raise Exception("This method should be implemented "
                        "for all Cloud environments.")

    @property
    def headers(self):
        return self._get_auth_headers()

    def _authenticate(self, *args, **kwargs):
        raise Exception("This method should be implemented "
                        "for all Cloud environments.")

    def _get_auth_headers(self):
        raise Exception("This method should be implemented "
                        "for all Cloud environments.")


class Databases(_HTTPClient):

    def __init__(self, beedbclient, enable_logging=False):
        self.client = beedbclient
        self.enable_logging = enable_logging

    def list(self):
        self.client.recent_response = self._get(
            self.client.beedb_endpoint + '/{0}/databases'.format(
                self.client.tenant),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != requests.codes.ok:
            raise exceptions.from_response(self.client.recent_response)

        _databases = json.loads(self.client.recent_response.content)
        databases = []
        for database in _databases['databases']:
            databases.append(DatabaseEntity(database))
        return databases

    def get(self, database):
        self.client.recent_response = self._get(
            self.client.beedb_endpoint + '/{0}/databases/{1}'.format(
                self.client.tenant, database),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != requests.codes.ok:
            raise exceptions.from_response(self.client.recent_response)

        _database = json.loads(self.client.recent_response.content)
        return DatabaseEntity(_database['database'])

    def create(self, name, role, description=None):
        data = {
            'database': name,
            'description': description,
            'role': role,
        }
        self.client.recent_response = self._post(
            self.client.beedb_endpoint +
            '/{0}/databases'.format(self.client.tenant),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger,
            data=data,
            json=data,
        )
        if self.client.recent_response.status_code != 202:
            raise exceptions.from_response(self.client.recent_response)

        _database = json.loads(self.client.recent_response.content)
        return DatabaseEntity(_database['database'])

    def delete(self, database):
        self.client.recent_response = self._delete(
            self.client.beedb_endpoint + '/{0}/databases/{1}'.format(
                self.client.tenant, database),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != 202:
            raise exceptions.from_response(self.client.recent_response)


class Roles(_HTTPClient):

    def __init__(self, beedbclient, enable_logging=False):
        self.client = beedbclient
        self.enable_logging = enable_logging

    def create(self, name, password, database_type, description=None):
        data = {
            'name': name,
            'description': description,
            'password': password,
            'database_type': database_type
        }
        self.client.recent_response = self._post(
            self.client.beedb_endpoint +
            '/{0}/roles'.format(self.client.tenant),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger,
            data=data,
            json=data,
        )
        if self.client.recent_response.status_code != 202:
            raise exceptions.from_response(self.client.recent_response)

        _role = json.loads(self.client.recent_response.content)
        return RoleEntity(_role['role'])

    def update(self, role_name, password):
        data = {
            'password': password,
        }
        self.client.recent_response = self._post(
            self.client.beedb_endpoint +
            '/{0}/roles/{1}'.format(self.client.tenant,
                                    role_name),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger,
            data=data,
            json=data,
        )
        if self.client.recent_response.status_code != requests.codes.ok:
            raise exceptions.from_response(self.client.recent_response)

        _role = json.loads(self.client.recent_response.content)
        return RoleEntity(_role['role'])

    def list(self):
        self.client.recent_response = self._get(
            self.client.beedb_endpoint + '/{0}/roles'.format(
                self.client.tenant),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != requests.codes.ok:
            raise exceptions.from_response(self.client.recent_response)

        _roles = json.loads(self.client.recent_response.content)
        roles = []
        for role in _roles['roles']:
            roles.append(RoleEntity(role))
        return roles

    def get(self, role_name):
        self.client.recent_response = self._get(
            self.client.beedb_endpoint + '/{0}/roles/{1}'.format(
                self.client.tenant, role_name),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != requests.codes.ok:
            raise exceptions.from_response(self.client.recent_response)

        _role = json.loads(self.client.recent_response.content)
        return RoleEntity(_role['role'])

    def delete(self, role_name):
        self.client.recent_response = self._delete(
            self.client.beedb_endpoint + '/{0}/roles/{1}'.format(
                self.client.tenant, role_name),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != 202:
            raise exceptions.from_response(self.client.recent_response)


class DatabaseTypes(_HTTPClient):

    def __init__(self, beedbclient, enable_logging=False):
        self.client = beedbclient
        self.enable_logging = enable_logging

    def list(self):
        self.client.recent_response = self._get(
            self.client.beedb_endpoint + '/{0}/databases/types'.format(
                self.client.tenant),
            headers=self.client.headers,
            verify=self.client.verify,
            logger=self.client.logger)

        if self.client.recent_response.status_code != requests.codes.ok:
            raise exceptions.from_response(self.client.recent_response)

        _types = json.loads(self.client.recent_response.content)
        types = []
        for _type in _types['database_types']:
            types.append(DatabaseTypeEntity(_type))
        return types


class BaseResourceObject(object):

    def __init__(self, json_object):
        self.json_object = json_object
        for name, value in json_object.items():
            setattr(self, name, value)


class DatabaseEntity(BaseResourceObject):
    pass


class RoleEntity(BaseResourceObject):
    pass


class DatabaseTypeEntity(BaseResourceObject):
    pass
